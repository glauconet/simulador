package com.simulador.mb;

import com.simulador.dao.UserBeanModel;
import com.simulador.dao.UserBeanModelDAO;
import com.simulador.pojo.Usuario;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.ArrayList;

@ManagedBean
@RequestScoped
public class ControleUsuario {

    private Usuario usuario;

    private String dados;

    public String getDados() {

        this.dados = usuario.toString();

        return dados;
    }



    public ControleUsuario(){

        this.usuario = new Usuario();

        userBeanModelControler = new UserBeanModelDAO();
        listaUsuario = new  ArrayList <UserBeanModel>();
    }

    public String recebeDados(){
        FacesMessage msg = new FacesMessage("Dados Recebidoo com Sucesso");

        FacesContext.getCurrentInstance().addMessage("",msg);
        return "usuario";
    }


    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }



    public UserBeanModelDAO userBeanModelControler ;

    public ArrayList <UserBeanModel> listaUsuario;

    public UserBeanModelDAO getUserBeanModelControler() {
        return userBeanModelControler;
    }

    public void setUserBeanModelControler(UserBeanModelDAO userBeanModelControler) {
        this.userBeanModelControler = userBeanModelControler;
    }



    
    public void validateEmail(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {
        String emailStr = (String) value;
        if (-1 == emailStr.indexOf("@")) {
            FacesMessage message = new FacesMessage("Invalid email address");
            throw new ValidatorException(message);
        }
    }

    public String addConfirmedUser() {

         UserBeanModel userBeanModel  = new UserBeanModel();

        // userBeanModel.setFirstName(this.getFirstName());
        // userBeanModel.setLastName(this.getLastName());
        // userBeanModel.setDob(this.getDob());
        // userBeanModel.setEmail(this.getEmail());
       //  userBeanModel.setSex(this.getSex());
       //  userBeanModel.setServiceLevel(this.getServiceLevel());

        userBeanModelControler.getListaUsuario().add(userBeanModel);

        // This method would call a database or other service and add the 
        // confirmed user information.
        // For now, we just place an informative message in request scope
        FacesMessage doneMessage = 
                new FacesMessage("Successfully added new user");
        FacesContext.getCurrentInstance().addMessage(null, doneMessage);
        return "done";
    }


    public ArrayList<UserBeanModel> recuperarUser(){

            return userBeanModelControler.getListaUsuario();
    }



}


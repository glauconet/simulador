package com.simulador.mb;

import com.simulador.pojo.PessoaOld;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class ControleTabela {

    private List<PessoaOld> lista;

    public ControleTabela(){

        lista = new ArrayList<PessoaOld>();

        lista.add(new PessoaOld(1,"Pessoa1","(99) 99999-9999"));
        lista.add(new PessoaOld(2,"Pessoa2","(99) 99999-9999"));
        lista.add(new PessoaOld(3,"Pessoa3","(99) 99999-9999"));
        lista.add(new PessoaOld(4,"Pessoa4","(99) 99999-9999"));
        lista.add(new PessoaOld(5,"Pessoa5","(99) 99999-9999"));
        lista.add(new PessoaOld(6,"Pessoa6","(99) 99999-9999"));
        lista.add(new PessoaOld(7,"Pessoa7","(99) 99999-9999"));
        lista.add(new PessoaOld(8,"Pessoa8","(99) 99999-9999"));
        lista.add(new PessoaOld(9,"Pessoa9","(99) 99999-9999"));
        lista.add(new PessoaOld(10,"Pessoa10","(99) 99999-9999"));

    }

    public String salvar(){
        for (PessoaOld obj : lista) obj.setEditando(false);
        return null;
    }

    public String excluir(PessoaOld obj){
        lista.remove(obj);
        return null;
    }


    public List<PessoaOld> getLista() {
        return lista;
    }

    public void setLista(List<PessoaOld> lista) {
        this.lista = lista;
    }










}

package com.simulador.mb;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * User: glauco
 * Date: 24/12/23
  */
@ManagedBean
@RequestScoped
public class ControladorPrincipal { // managed bean principal

    private String mensagen;
    private String dataHoraEvento;
    private SimpleDateFormat sdf;

    public ControladorPrincipal() {

        sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:S");
        dataHoraEvento = sdf.format(Calendar.getInstance().getTime());
        mensagen = "Hora de Inicio do Sistema" ;

    }

    public String retorno(){

            this.mensagen = "Você foi redirecionado de maneira dinâmica" ;
        return "main";

    }

    public String getDataHoraEvento() {
        return dataHoraEvento;
    }

    public void setDataHoraEvento(String dataHoraEvento) {
        this.dataHoraEvento = dataHoraEvento;
    }

    public String getMensagen() {
        return mensagen;
    }

    public void setMensagen(String mensagen) {
        this.mensagen = mensagen;
    }

    public String onErro(){

        return  "error";
    }




}

package com.simulador.mb;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class ControleComponentes implements Serializable{

    private String texto;
    private String idComponente;

    public ControleComponentes(){
    }


    public String executar(){

        return "componentes2";
    }

    public void listener(ActionEvent event) {

        UIComponent source = event.getComponent();
        idComponente = source.getId() ;
    }


    public String geraTexto(){
        if(this.texto ==null){
           this.texto = "Gerado um novo texto, se chamar de novo vai apagar";
        }else{
           this.texto = null;
        }
      return "componentes";
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(String idComponente) {
        this.idComponente = idComponente;
    }
}

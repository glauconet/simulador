package com.simulador.mb;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class ControleFormulario implements Serializable{

    private Integer id;
    private String senha;
    private String nome;
    private String obs;
    private String grau;
    private String saida;
    private List<String> listaGrau = new ArrayList<String>();

    public ControleFormulario() {
        id = 1;

        listaGrau.add("Ensino Fundamental");
        listaGrau.add("Ensino Médio");
        listaGrau.add("Ensino Superior");
    }

    public String processa(){
        this.saida = toString() ;
        return "componentes3";
    }

    public String processa2(){
        this.saida = toString() ;
        return "componentes4";
    }

    public String getSaida() {
        return saida;
    }

    public void setSaida(String saida) {
        this.saida = saida;
    }

    @Override
    public String toString() {
        return "Dados" +
                " id     = " + id +
                " senha  = " + senha    + "<br />" +
                " nome   = " + nome     + "<br />"  +
                " obs    = " + obs      + "<br />"  +
                " grau   = " + grau     + "<br />"
        ;
    }

    public List<String> getListaGrau() {
        return listaGrau;
    }

    public void setListaGrau(List<String> listaGrau) {
        this.listaGrau = listaGrau;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getGrau() {
        return this.grau;
    }

    public void setGrau(String grau) {
        this.grau = grau;
    }
}

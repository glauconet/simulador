package com.simulador.mb;

import com.simulador.bc.PerguntaBusinessControler;
import com.simulador.pojo.PerguntaPojo;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.ArrayList;

@ManagedBean
@RequestScoped
public class PerguntaManagedBean {

    protected PerguntaBusinessControler perguntaBC = new PerguntaBusinessControler();

    public PerguntaPojo perguntaPojo;

    public PerguntaManagedBean(){

        perguntaPojo  = new PerguntaPojo();

    }

    public PerguntaPojo getPerguntaPojo() {
        return perguntaPojo;
    }

    public void setPerguntaPojo(PerguntaPojo perguntaPojo) {
        this.perguntaPojo = perguntaPojo;
        System.out.println("Setting PerguntaPojo");
        System.out.println(perguntaPojo.toString() );
    }

    public String addPergunta() {

        System.out.print("addPergunta  inserir ou atualiza -" + '\n' );

        System.out.print(perguntaPojo.toString());

        perguntaBC.inserirPergunta(perguntaPojo);

        FacesMessage doneMessage =  new FacesMessage("Gravou Pergunta com sucesso");

        FacesContext.getCurrentInstance().addMessage(null, doneMessage);
        return "listarPergunta2";
    }

    //public  ArrayList<PerguntaPojo>  getListaPerguntas(){
       // return perguntaBC.pegarListaPerguntas();
  //  }

    public  ArrayList<PerguntaPojo>  getListaPerguntasJPA(){
        return perguntaBC.pegarListaPerguntasJPA();
    }


    
    public void validateEmail(FacesContext context, UIComponent toValidate,
            Object value) throws ValidatorException {
        String emailStr = (String) value;
        if (-1 == emailStr.indexOf("@")) {
            FacesMessage message = new FacesMessage("Invalid email address");
            throw new ValidatorException(message);
        }
    }

    public String editarPergunta(PerguntaPojo perguntaPojo) {

        this.perguntaPojo =  perguntaPojo;

        return "registerPergunta";
    }

    public String excluirPergunta(PerguntaPojo perguntaPojo){

       // this.perguntaPojo =  perguntaPojo;
        System.out.print("Sera excluido" );
        System.out.print(perguntaPojo);

        perguntaBC.removerPergunta(perguntaPojo);

        return "listarPergunta";
    }



    public String editarPergunta() { // usado com com o setPropertyActionListener

        return "registerPergunta";
    }

    public String excluirPergunta() {  // usado com com o setPropertyActionListener

        System.out.print("Sera excluido" );
        System.out.print(perguntaPojo);

        return "listarPergunta";
    }





}


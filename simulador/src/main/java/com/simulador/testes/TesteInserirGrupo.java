package com.simulador.testes;

import com.simulador.conexao.EntityManagerUtil;
import com.simulador.pojo.Grupo;

import javax.persistence.EntityManager;
public class TesteInserirGrupo {  //   exemplo do curso de jsf 2 devmedia

	public static void main(String[] args) {
		// 
		EntityManager em = EntityManagerUtil.getEntityManager();
		Grupo g = new Grupo();
		g.setNome("Gestores");
		em.getTransaction().begin();
		em.persist(g);
		em.getTransaction().commit();
	}

}

package com.simulador.conexao;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConexaoJPA {


    private static EntityManagerFactory entityManagerFactory
            = Persistence.createEntityManagerFactory("simulador");

    public static EntityManager getEntityManager(){
        return entityManagerFactory.createEntityManager();
    }




}

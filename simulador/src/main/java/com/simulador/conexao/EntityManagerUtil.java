package com.simulador.conexao;

//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil { // exemplo do curso de jsf 2 devmedia
		private static EntityManagerFactory factory = null;
		private static EntityManager em = null;
		
		public static EntityManager getEntityManager(){
			if (factory == null){
				factory = Persistence.createEntityManagerFactory("simulador");
			}
			if (em == null){
				em = factory.createEntityManager();
			}
			return em;			
		}
		
}

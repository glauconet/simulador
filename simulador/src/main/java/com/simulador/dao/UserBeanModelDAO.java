package com.simulador.dao;

import java.util.ArrayList;
import com.simulador.pojo.Usuario;


public class UserBeanModelDAO {

    ArrayList<UserBeanModel>  listaUsuario;

    ArrayList<Usuario>  listaUsuarioPojo;

    public UserBeanModelDAO(){

        listaUsuario = new ArrayList<UserBeanModel>();
    }

    public ArrayList<UserBeanModel> getListaUsuario() {
        return listaUsuario;
    }

    public void addUsuario(UserBeanModel userBeanModel){

        listaUsuario.add(userBeanModel);

    }

    public void addUsuario(Usuario userBeanPojo){

        listaUsuarioPojo.add(userBeanPojo);

    }


}


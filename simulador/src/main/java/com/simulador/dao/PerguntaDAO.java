package com.simulador.dao;

import com.simulador.pojo.PerguntaPojo;

import javax.persistence.EntityManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class PerguntaDAO  {


   // ArrayList<PerguntaPojo>  listaPerguntaPojo;
   // Connection con;
    private EntityManager entityManager;

    public PerguntaDAO(EntityManager em){

        this.entityManager = em;


    }

    public void adicionar(PerguntaPojo perguntaPojo){

       this.entityManager.getTransaction().begin();

       this.entityManager.persist(perguntaPojo);   // persist

       this.entityManager.getTransaction().commit();

    }



    public void atualizar(PerguntaPojo perguntaPojo){

        this.entityManager.getTransaction().begin();

        this.entityManager.find(PerguntaPojo.class, perguntaPojo.getId());

        this.entityManager.merge(perguntaPojo);  // merge

        this.entityManager.getTransaction().commit();

    }

    public ArrayList<PerguntaPojo> listarJPA(){

       //this.entityManager.getTransaction().begin();


        ArrayList<PerguntaPojo> perguntas;

        perguntas = (ArrayList<PerguntaPojo>)  this.entityManager.createQuery("select a from PerguntaPojo a").getResultList();

        //em.close();

        //this.entityManager.close();
        return perguntas;

    }


    public void inserir(PerguntaPojo perguntaPojo) {
        System.out.println("I will insert This --------------- >INSERIR");
        System.out.println(perguntaPojo.toString());

        if(getId(perguntaPojo.getId())){
            System.out.println("I will insert This ----------- >ATUALIZARr");
            atualizar(perguntaPojo);

        }else{
            System.out.println("I will insert This ----------- >SALVAR");
           salvar(perguntaPojo);
        } //listaPerguntaPojo.add(perguntaPojo);
    }



    public boolean getId(int valor)  {

        System.out.println("I will insert This ----------- >BUSCAR código pergunta" + valor);
        PreparedStatement consulta = null;
        ResultSet resultado = null;

        String sql = "select * from pergunta where id = ?";

        Boolean retorno = null;
        try {
           // consulta = con.prepareStatement(sql);
            consulta.setInt(1, valor);
            resultado = consulta.executeQuery();

            if (resultado.next()) {
                retorno = true;
            }else{
                retorno = false;
            }
        } catch (SQLException e) {
            System.out.println("Erro ao buscar código do contato. Mensagem: " + e.getMessage());
        } finally {
            try {
                consulta.close();
                resultado.close();
            } catch (Throwable e) {
                System.out.println("Erro ao fechar operações de consulta. Mensagem: " + e.getMessage());
            }
        }
        return retorno;
    }

    public void salvar(PerguntaPojo perguntaPojo) {
        //Connection conexao = this.geraConexao();
        PreparedStatement insereSt = null;

        String sql = "insert into pergunta(id, pergunta, complemento, resposta, data, valor) values(?, ?, ?, ?, ?,?)";

        try {
           // insereSt = con.prepareStatement(sql);
            insereSt.setInt(1, perguntaPojo.getId());
            insereSt.setString(2, perguntaPojo.getPergunta());
            insereSt.setString(3, perguntaPojo.getComplemento());
            insereSt.setString(4, perguntaPojo.getResposta());
            insereSt.setDate(5, null);
            insereSt.setInt(6,  perguntaPojo.getValor());
            insereSt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Erro ao incluir contato. Mensagem: " + e.getMessage());
        } finally {
            try {
                insereSt.close();
               // con.close();
            } catch (Throwable e) {
                System.out.println("Erro ao fechar opera��es de inser��o. Mensagem: " + e.getMessage());
            }
        }
    }

    public void atualizarOld(PerguntaPojo perguntaPojo) {

        PreparedStatement atualizaSt = null;

        //Aqui n�o atualizamos o campo data de cadastro
        String sql = "update pergunta set pergunta=?, complemento=?, resposta=?, data=?, valor = ? where id=?";

        try {
          //  atualizaSt = con.prepareStatement(sql);
            atualizaSt.setString(1, perguntaPojo.getPergunta());
            atualizaSt.setString(2, perguntaPojo.getComplemento());
            atualizaSt.setString(3, perguntaPojo.getResposta());
            atualizaSt.setDate(4, null);
            atualizaSt.setInt(5, perguntaPojo.getValor());
            atualizaSt.setInt(6, perguntaPojo.getId());
            atualizaSt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Erro ao atualizar contato. Mensagem: " + e.getMessage());
        } finally {
            try {
                atualizaSt.close();
                //con.close();
            } catch (Throwable e) {
                System.out.println("Erro ao fechar opera��es de atualiza��o. Mensagem: " + e.getMessage());
            }
        }
    }

    public PerguntaPojo buscaContato(int valor)  {

        PreparedStatement consulta = null;
        ResultSet resultado = null;
        PerguntaPojo perguntaPojo = null;

        String sql = "select * from pergunta where codigo = ?";

        try {
           // consulta = con.prepareStatement(sql);
            consulta.setInt(1, valor);
            resultado = consulta.executeQuery();

            if (resultado.next()) {
                perguntaPojo = new PerguntaPojo();
                perguntaPojo.setId(new Integer(resultado.getInt("id")));
                perguntaPojo.setPergunta(resultado.getString("pergunta"));
                perguntaPojo.setComplemento(resultado.getString("complemento"));
                perguntaPojo.setData(resultado.getDate("data"));


            }
        } catch (SQLException e) {
            System.out.println("Erro ao buscar c�digo do contato. Mensagem: " + e.getMessage());
        } finally {
            try {
                consulta.close();
                resultado.close();

            } catch (Throwable e) {
                System.out.println("Erro ao fechar opera��es de consulta. Mensagem: " + e.getMessage());
            }
        }
        return perguntaPojo;
    }



     public void excluir(PerguntaPojo perguntaPojo){

         this.entityManager.getTransaction().begin();

         this.entityManager.remove(perguntaPojo);


         this.entityManager.getTransaction().commit();

     }









    //public void excluir(PerguntaPojo perguntaPojo) {
       /* Connection conexao = this.geraConexao();
        PreparedStatement excluiSt = null;

        String sql = "delete from contato where codigo = ?";

        try {
            excluiSt = conexao.prepareStatement(sql);
            excluiSt.setInt(1, contato.getCodigo().intValue());
            excluiSt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Erro ao excluir contato. Mensagem: " + e.getMessage());
        } finally {
            try {
                excluiSt.close();
                conexao.close();
            } catch (Throwable e) {
                System.out.println("Erro ao fechar opera��es de exclus�o. Mensagem: " + e.getMessage());
            }
        }    */
   // }

    public ArrayList<PerguntaPojo> listar() {

       // Connection conexao = this.geraConexao();

        ArrayList<PerguntaPojo> perguntas = new ArrayList<PerguntaPojo>();

        Statement consulta = null;
        ResultSet resultado = null;
        PerguntaPojo perguntaPojo = null;
        String sql = "select * from pergunta";

        try {
          //  consulta = conexao.createStatement();
            resultado = consulta.executeQuery(sql);

            while (resultado.next()) {
                perguntaPojo = new PerguntaPojo();

                perguntaPojo.setId(resultado.getInt("id"));
                perguntaPojo.setPergunta(resultado.getString("pergunta"));
                perguntaPojo.setComplemento(resultado.getString("complemento"));
                perguntaPojo.setResposta(resultado.getString("resposta"));
                perguntaPojo.setData(resultado.getDate("data"));
                perguntaPojo.setValor(resultado.getInt("valor"));

                perguntas.add(perguntaPojo);
            }
        } catch (SQLException e) {
            System.out.println("Erro ao buscar pergunta do contato. Mensagem: " + e.getMessage());
        } finally {
            try {
                consulta.close();
                resultado.close();
              //  conexao.close();
            } catch (Throwable e) {
                System.out.println("Erro ao fechar operaçõess de consulta. Mensagem: " + e.getMessage());
            }
        }
        return  perguntas;
    }


      /*

    public ArrayList<PerguntaPojo> getListaPerguntaPojo() {

        ArrayList<PerguntaPojo> lista = new ArrayList<PerguntaPojo>()  ;
        for (PerguntaPojo item: listaPerguntaPojo)  {

            PerguntaPojo pojo =  new PerguntaPojo();

            pojo.setPergunta(item.getPergunta());
            pojo.setComplemento(item.getComplemento());
            pojo.setData(item.getData());
           // pojo.setEmail(item.getEmail());
            pojo.setResposta(item.getResposta());
            pojo.setValor(item.getValor());

            lista.add(pojo);
        }

        return lista;
    }          */








}


package com.simulador.bc;

import com.simulador.conexao.ConexaoJPA;
import com.simulador.dao.PerguntaDAO;
import com.simulador.pojo.PerguntaPojo;

import javax.persistence.EntityManager;
import java.util.ArrayList;

public class PerguntaBusinessControler{     // classe de controle de negócios


    EntityManager entityManager = ConexaoJPA.getEntityManager();

    public PerguntaDAO perguntaDAO = new PerguntaDAO(entityManager);

    public void inserirPergunta(PerguntaPojo perguntaPojo){

         if(perguntaPojo.getId()==null){

             System.out.print("----------- adicionar -----------");

            perguntaDAO.adicionar(perguntaPojo);

         }else{
             System.out.print("----------- atualizar ----------");

             perguntaDAO.atualizar(perguntaPojo);

         }

    }

   // public ArrayList<PerguntaPojo> pegarListaPerguntas(){
    //   return perguntaDAO.listar(); //.getListaPerguntaPojo();
   // }


    public ArrayList<PerguntaPojo> pegarListaPerguntasJPA(){
        return perguntaDAO.listarJPA(); //.getListaPerguntaPojo();
    }

    public void removerPergunta(PerguntaPojo perguntaPojo){

        perguntaDAO.excluir(perguntaPojo);
    }
}


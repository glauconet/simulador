package com.simulador.pojo;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Digits;


@Entity
public class Usuario {
    @Id
    private String id;

    @NotEmpty(message = "O nome deve ser informado")
    @Length(max = 40, message = " o nome deve ter no máximo 40 caracteres")
    protected String nome;

    @NotEmpty(message = "A senha deve ser informada")
    @Length(min =6 , max = 8, message = "A senha deve ter entre {min} e {max} caracteres")
    protected String senha;

    @NotEmpty(message = "O cpf deve ser informado")
    @CPF(message="Informe um cpf válido")
    protected String cpf;

    @Digits(fraction=2,integer=10,message = " Digite um valor válido com fração")
    protected Double renda;

    @NotEmpty(message = "O e-mail deve ser informado")
    @Email(message = "informe um e-mail válido")
    protected String email;

    //@NotEmpty(message = "Sua página  de ser informada ")
    @URL(protocol = "http", message = "tem de ter http")
    protected String pagina;


    public Usuario() {
       // this.id = id;
        this.nome = "";
        this.senha = "";
        this.cpf = "";
        this.renda = null;
        this.email = "";
        this.pagina = "";
    }

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Double getRenda() {
        return renda;
    }

    public void setRenda(Double renda) {
        this.renda = renda;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id='" + id + '\'' +
                ", nome='" + nome + '\'' +
                ", senha='" + senha + '\'' +
                ", cpf='" + cpf + '\'' +
                ", renda=" + renda +
                ", email='" + email + '\'' +
                ", pagina='" + pagina + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }




}


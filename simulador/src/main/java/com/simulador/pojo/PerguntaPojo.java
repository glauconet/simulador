package com.simulador.pojo;

import javax.persistence.*;
import java.util.Date;

@Table(name="pergunta")
@Entity
public class  PerguntaPojo {

    @Id @GeneratedValue(strategy= GenerationType.AUTO)
    private  Integer id;
    private  String pergunta;
    private  String complemento;
    private  String resposta;
    private  Date   data;
    private  Integer valor;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String firstName) {
        this.pergunta = firstName;
    }

    public String getComplemento() {
        return complemento;
    }

    @Override
    public String toString() {
        return "PerguntaPojo \n" +
                " id    =   " + id + '\n' +
                " pergunta  =   " + pergunta + '\n' +
                " complemento   =   " + complemento + '\n' +
                " data  =   " + data +    '\n' +
                " resposta  =   " + resposta + '\n' +
                " valor =   " + valor + '\n' +
                '\n';
    }

    public void setComplemento(String lastName) {
        this.complemento = lastName;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date dob) {
        this.data = dob;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String sex) {
        this.resposta = sex;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer serviceLevel) {
        this.valor = serviceLevel;
    }
    



}

